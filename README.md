Simple hash service

(The outputs are a bit inconsistent re: text vs json vs silence)

Initial keyring is read from DynamoDB
(obvs. you need AWS env. credentials + a DynamoDB table)

To hash an ID into our bits:

    curl endpoint/hash/22
    {"hash":"439acee49580e07dae64a048c983ecb8","key":"OdA5q","id":22}

To check a hash

    curl endpoint/check/22/OdA5q/439acee49580e07dae64a048c983ecb8
    OK

To refresh the keys from DynamoDB

    curl endpoint/refresh
    [no output]

To add a new key to the existing set

    curl endpoint/newkey
    OK E4fW6

If a requested keyID isn't present locally, it'll be retrieved from
DynamoDB if possible.
